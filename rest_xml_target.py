#!/usr/bin/env python
#
# Rest Target for Peach Class
#

'''
Copyright 2020 GitLab B.v.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import xml.etree.ElementTree as ET
from flask import make_response, Flask, request
from flask_restful import Resource, Api, abort
from werkzeug.exceptions import HTTPException
import sqlite3
import logging
import logging.handlers
import random

logger = logging.getLogger(__name__)


def output_xml(data, code, headers=None):
    """Makes a Flask response with a XML encoded body"""

    logger.info("output_xml(data, code, headers)")

    if isinstance(data, str):
        resp = make_response(data, code)
    elif str(type(data)) == "<class 'xml.etree.ElementTree.ElementTree'>":
        try:
            # wrap in 'response' tag
            existing_root = data.getroot()
            new_root = ET.Element('response')

            data._setroot(new_root)
            new_root.append(existing_root)

            # convert to string
            xml_str = ET.tostring(data.getroot(), encoding='utf-8').decode('utf-8')

            # response
            resp = make_response(xml_str, code)
        except:
            logger.warning("output_xml: failed to make response")
            raise
    elif isinstance(data, dict):
        doc = ET.ElementTree(ET.fromstring('<error/>'))
        doc.getroot().text = data['message']
        xml_str = ET.tostring(doc.getroot(), encoding='utf-8').decode('utf-8')

        resp = make_response(xml_str, code)

    else:
        logger.error("output_xml: Unknown type: %s" % type(data))
        raise Exception("output_xml: Unknown type: %s" % type(data))

    resp.headers.extend(headers or {})
    return resp


app = Flask(__name__)
api = Api(app, default_mediatype='application/xml')
api.representations['application/xml'] = output_xml


@app.route("/")
def Home():
    with open('rest_target.html', 'r') as myfile:
        data = myfile.read()
    
    return data, 200, {'Content-Type': 'text/html'}


@app.route("/api/.htaccess")
def FakeHtAccess():
    data = "look out, it's and .htaccess file!"
    return data, 200, {'Content-Type': 'text/html'}


class ApiRoot(Resource):
    def get(self):
        return [
            '/api/users'
        ]


def xml_SubElement(parent, tag, text):
    '''Helper function for adding sub-elements
    '''
    child = ET.SubElement(parent, tag)
    child.text = text
    return child


class ApiUsers(Resource):
    
    def validateToken(self):
        try:
            if request.headers.get('Authorization') == "Token b5638ae7-6e77-4585-b035-7d9de2e3f6b3":
                return True
        except:
            pass
        
        return False
    
    def get(self):
        # Comment out auth check to trigger chail failure
        
        logger.info("Getting all users")
        users = ET.ElementTree(ET.fromstring('<users/>'))
        
        with GetConnection() as conn:
            try:
                c = conn.cursor()
                for row in c.execute("select user_id, user, first, last, password from users"):
                    user = ET.SubElement(users.getroot(), 'user')
                    xml_SubElement(user, 'user_id', str(row[0]))
                    xml_SubElement(user, 'user', row[1])
                    xml_SubElement(user, 'first', row[2])
                    xml_SubElement(user, 'last', row[3])
                    xml_SubElement(user, 'password', row[4])
                    
            except HTTPException as e:
                raise e
            except Exception as e:
                logger.error('Error getting users: ' + str(e))
                abort(500)
        
        # Trigger sensitive information disclosure checks
        if random.randint(0, 10) == 1:
            flip = random.randint(0, 1)
            if flip == 0:
                return "Blah blah blah. Powered by: ASPX.NET Other other other", 200, {'Content-Type': 'text/html'}
            elif flip == 1:
                return "Blah blah blah. Version: 1.1.1 Other other other", 200, {'Content-Type': 'text/html'}
            
            return "Blah blah blah Stack trace: xxxxxx Ohter other other", 200, {'Content-Type': 'text/html'}
        
        return users
        
    def post(self):
        if not self.validateToken():
            abort(401)
        
        logger.info(request.get_data())
        data = ET.ElementTree(ET.fromstring(request.get_data()))
        user = data.getroot()
        
        logger.info("Creating new user '%s'" % user.find("user").text)
        
        user_id = -1
        with GetConnection() as conn:
            try:
                c = conn.cursor()
                c.execute("insert into users (user, first, last, password) values ('%s', '%s', '%s', '%s')" % (
                    user.find('user').text, 
                    user.find('first').text, 
                    user.find('last').text, 
                    user.find('password').text))
                user_id = c.lastrowid
                conn.commit()
                
                ret = ET.ElementTree(ET.fromstring("<user_id/>"))
                ret.getroot().text = str(user_id)

                return ret, 201
            
            except HTTPException as e:
                raise e
            except Exception as e:
                logger.error('Error creating user: ' + str(e))
                abort(500)
    
    def delete(self):
        if not self.validateToken():
            abort(401)
        
        user = request.args.get('user')
        
        logging.info("Deleting user %s" % user)
        
        try:
            with GetConnection() as conn:
                c = conn.cursor()
                c.execute("delete from users where user = '%s'" % user)
                
                if c.rowcount == 0:
                    abort(404, message="User not found.")
                
                conn.commit()
            
            ret = ET.ElementTree(ET.fromstring("<user/>"))
            ret.getroot().text = user

            return ret, 204
        
        except HTTPException as e:
            raise e
        except Exception as e:
            logger.error('Error deleting user: %s %s' % (user, str(e)))
            abort(500, message="Error deleteing user")


class ApiUser(Resource):
    def validateToken(self):
        try:
            if request.headers.get('Authorization') == "Token b5638ae7-6e77-4585-b035-7d9de2e3f6b3":
                return True
        except:
            pass
        
        return False
    
    def get(self, user_id):
        if not self.validateToken():
            abort(401)
        
        logging.info("Getting user %d" % user_id)
        
        try:
            with GetConnection() as conn:
                c = conn.cursor()
                for row in c.execute("select user_id, user, first, last, password from users where user_id = %d" % user_id):
                    
                    user = ET.ElementTree(ET.fromstring("<user/>"))
                    root = user.getroot()
                    xml_SubElement(root, 'user_id', str(row[0]))
                    xml_SubElement(root, 'user', row[1])
                    xml_SubElement(root, 'first', row[2])
                    xml_SubElement(root, 'last', row[3])
                    xml_SubElement(root, 'password', row[4])
                    xml_SubElement(root, 'html', "<b>%s</b>" % row[3])

                    return user
                
                if c.rowcount == 0:
                    abort(404, message="User not found.")
            
        except HTTPException as e:
            raise e
        except Exception as e:
            logger.error('Error getting user_id %d: %s' % (user_id, str(e)))
            abort(500)
    
    def put(self, user_id):

        if not self.validateToken():
            abort(401)
        
        data = ET.ElementTree(ET.fromstring(request.get_data()))
        user = data.getroot()
        
        logger.info("Updating user_id %d" % user_id)
        
        try:
            with GetConnection() as conn:
                c = conn.cursor()
                c.execute("update users set user = '%s', first = '%s', last = '%s', password = '%s' where user_id = %d" % (
                    user.find('user').text, 
                    user.find('first').text, 
                    user.find('last').text, 
                    user.find('password').text, 
                    user_id))
                
                if c.rowcount == 0:
                    logger.warning("User id not found while updating %d" % user_id)
                    abort(404, message="User not found.")
                
                conn.commit()

            return data

        except HTTPException as e:
            raise e
        except Exception as e:
            logger.error('Error creating user: ' + str(e))
            abort(500)
    
    def delete(self, user_id):
        if not self.validateToken():
            abort(401)
        
        logging.info("Deleting user %d" % user_id)
        
        try:
            with GetConnection() as conn:
                c = conn.cursor()
                c.execute("delete from users where user_id = %d" % user_id)
                
                if c.rowcount == 0:
                    abort(404, message="User not found.")
                
                conn.commit()
            
            ret = ET.ElementTree(ET.fromstring('<user_id/>'))
            ret.getroot().text = str(user_id)

            return ret, 204
        
        except HTTPException as e:
            raise e
        except Exception as e:
            logger.error('Error deleting user_id: %s %s' % (user_id, str(e)))
            abort(500, message="Error deleteing user")


api.add_resource(ApiRoot, '/api')
api.add_resource(ApiUsers, '/api/users')
api.add_resource(ApiUser, '/api/users/<int:user_id>')


def GetConnection():
    return sqlite3.connect("rest_xml_target.db")


def CreateDb():
    logger.info("Creating in-memory database.")
    try:
        with GetConnection() as conn:
            c = conn.cursor()
            c.execute('drop table if exists users')
            c.execute('''create table users (user_id integer primary key, user text unique, first text, last text, password text)''')
            c.execute('''insert into users (user, first, last, password) values ('admin', 'Joe', 'Smith', 'Password!')''')

            user_id = str(c.lastrowid)

            c.execute('drop table if exists msgs')
            c.execute('''create table msgs (msg_id integer primary key, from_id int, to_id int, subject text, msg text)''')
            c.execute('''insert into msgs (from_id, to_id, subject, msg) values (''' + user_id + ''',''' + user_id + ''', 'Hello From Myself', 'Welcome to the system...!')''')
            conn.commit()

    except Exception as e:
        logger.error('Error creating user: ' + str(e))
        raise e


if __name__ == '__main__':
    logger.setLevel(logging.DEBUG)
    
    logFormatter = logging.Formatter("%(asctime)s [%(levelname)-5.5s] %(message)s")
    syslogHandler = logging.handlers.SysLogHandler()
    syslogHandler.setFormatter(logFormatter)
    logger.addHandler(syslogHandler)
    
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)
    
    fileHandler = logging.FileHandler('rest_xml_target.log')
    fileHandler.setFormatter(logFormatter)
    logger.addHandler(fileHandler)

    logger.info("rest_xml_target.py initializing.")
    CreateDb()
    logger.info("Starting REST XML application")
    app.run(debug=True, host="0.0.0.0", port=6666)

# end
