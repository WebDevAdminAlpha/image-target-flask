FROM python:3.8

RUN \
    apt-get update \
    && apt-get install -y \
    build-essential

EXPOSE 7777
EXPOSE 7778

ENV TZ="America/Los_Angeles"

WORKDIR /app
ADD . /app

RUN python -m pip install -r requirements.txt

CMD python /app/rest_target.py
